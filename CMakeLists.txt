cmake_minimum_required(VERSION 2.8.11)
project(foo)

find_package(ECM 0.0.8 REQUIRED NO_MODULE)
include(FeatureSummary)
include(WriteBasicConfigVersionFile)

# where to look first for cmake modules, before ${CMAKE_ROOT}/Modules/ is checked
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ECM_MODULE_PATH} ${ECM_KDE_MODULE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
include(ECMMarkNonGuiExecutable)

include(KDEInstallDirs)
include(KDECMakeSettings)

include(ECMOptionalAddSubdirectory)

find_package(Qt5 CONFIG REQUIRED COMPONENTS Core)
find_package(KF5 REQUIRED COMPONENTS Archive)

# Set source files
set(foo_SRCS main.cpp)

# Tell CMake to create the helloworld executable
add_executable(foo ${foo_SRCS})

# Use the Widgets module from Qt 5.
target_link_libraries(foo Qt5::Core)
