#include <QtCore/QCoreApplication>

#include <KF5/KArchive/K7Zip>

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    return app.exec();
}
